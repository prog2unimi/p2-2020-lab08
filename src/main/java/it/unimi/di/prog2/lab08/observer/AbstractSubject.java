package it.unimi.di.prog2.lab08.observer;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractSubject<T> implements Subject<T> {
  protected T state;
  private List<Observer<T>> observers = new ArrayList<>();

  @Override
  public void registerObserver(Observer<T> o) {
    observers.add(o);
  };

  @Override
  public void removeObserver(Observer<T> o){
    observers.remove(o);
  };

  @Override
  public void notifyObservers(){
    for (Observer<T> observer : observers) {
      observer.update(this, state);
    }
  };

  @Override
  public T getState(){
    return state;
  };

  @Override
  public void setState(T value) {
    if (state != value) {
      state = value;
      notifyObservers();
    }
  };
}
