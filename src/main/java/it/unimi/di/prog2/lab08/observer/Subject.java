package it.unimi.di.prog2.lab08.observer;

public interface Subject<T> {
  void registerObserver(Observer<T> o);
  void removeObserver(Observer<T> o);
  void notifyObservers();

  T getState();
  void setState(T value);
}
