package it.unimi.di.prog2.lab08.observer;

public interface Observer<T>{
  void update(Subject<T> model, T state);
}
