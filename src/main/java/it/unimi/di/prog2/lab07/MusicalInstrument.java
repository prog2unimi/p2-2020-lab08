package it.unimi.di.prog2.lab07;

public interface MusicalInstrument {
  String play();
  default int numInstruments() {
    return 1;
  };
}
