package it.unimi.di.prog2.lab07;

public class GermanMusicInstrument implements MusicalInstrument {
  private GermanPercussiveInstrument instrument;

  public GermanMusicInstrument(GermanPercussiveInstrument instrument) {
    this.instrument = instrument;
  }

  @Override
  public String play() {
    StringBuffer result = new StringBuffer();

    result.append(instrument.spiel());
    result.append(instrument.spiel());
    result.append("ng");

    return result.toString();
  }
}
