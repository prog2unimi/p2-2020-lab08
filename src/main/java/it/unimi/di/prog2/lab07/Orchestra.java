package it.unimi.di.prog2.lab07;

import java.util.ArrayList;
import java.util.List;

public class Orchestra implements MusicalInstrument {
  List<MusicalInstrument> lista = new ArrayList<>();

  public void add(MusicalInstrument instrument) {
    lista.add(instrument);
  }

  @Override
  public int numInstruments() {
    int numTotale = 0;
    for (MusicalInstrument instrument : lista) {
      numTotale += instrument.numInstruments();
    }
    return numTotale;
  }

  @Override
  public String play() {
    StringBuilder suonoTotale = new StringBuilder();

    boolean first = true;
    for (MusicalInstrument instrument : lista) {
      if (!first)
        suonoTotale.append("\n");
      else
        first = false;
      suonoTotale.append(instrument.play());

    }
    return suonoTotale.toString();
  }
}
