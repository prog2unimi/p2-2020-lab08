package it.unimi.di.prog2.lab07;

public class MusicalInstrumentCounter implements MusicalInstrument {
  private static int counter;
  private MusicalInstrument instrument;

  public MusicalInstrumentCounter(MusicalInstrument instrument) {
    this.instrument = instrument;
  }

  public static int getCount() {
    return counter;
  }

  public static void resetCount() {
    counter = 0;
  }

  @Override
  public int numInstruments() {
    return instrument.numInstruments();
  }

  @Override
  public String play() {
    counter += instrument.numInstruments();
    return instrument.play();
  }

}
