package it.unimi.di.prog2.lab07;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class MusicTest {

  @Before
  public void resetCounter() throws Exception {
    MusicalInstrumentCounter.resetCount();
  }

  @Test
  public void testHorn() {
    MusicalInstrument instrument = new Horn();

    String suono = instrument.play();

    assertEquals("papapa", suono);
  }

  @Test
  public void testTrumpet() {
    Trumpet instrument = new Trumpet();

    String suono = instrument.play();

    assertEquals("pepepe", suono);
  }

  @Test
  public void testWaterGlass() {
    WaterGlass glass = new WaterGlass();

    String suono = glass.tap();

    assertEquals("diding", suono);
  }

  @Test
  public void testIronRod() {
    GermanPercussiveInstrument germanInstrument = new IronRod();

    String suono = germanInstrument.spiel();

    assertEquals("ta", suono);
  }

  @Test
  public void testClassAdapter() {
    MusicalInstrument instrument = new WaterGlassInstrument();

    String suono = instrument.play();

    assertEquals("diding", suono);
  }

  @Test
  public void testObjectAdapter() {
    MusicalInstrument instrument = new GermanMusicInstrument(new IronRod());

    String suono = instrument.play();

    assertEquals("tatang", suono);
  }

  @Test
  public void testPolimorfismo() {
    List<MusicalInstrument> orchestra = new ArrayList<>();

    orchestra.add(new Trumpet());
    for (int i = 0; i < 4; i++) {
      orchestra.add(new Horn());
    }
    orchestra.add(new WaterGlassInstrument());
    orchestra.add(new GermanMusicInstrument(new IronRod()));

    StringBuilder suonoTotale = new StringBuilder();
    for (MusicalInstrument instrument : orchestra) {
      suonoTotale.append(instrument.play());
      suonoTotale.append('\n');
    }

    assertEquals("pepepe\npapapa\npapapa\npapapa\npapapa\ndiding\ntatang\n",
        suonoTotale.toString());
  }

  @Test
  public void testCounterDecorator() {

    MusicalInstrument instrument = new MusicalInstrumentCounter(new Trumpet());
    MusicalInstrument otherInstrument = new MusicalInstrumentCounter(new Trumpet());

    for (int i = 0; i < 3; i++) {
      instrument.play();
      otherInstrument.play();
    }
    assertEquals(6, MusicalInstrumentCounter.getCount());

  }

  @Test
  public void testCounterDecorator2() {

    MusicalInstrument instrument = new MusicalInstrumentCounter(new Horn());
    MusicalInstrument instrumentAdapted = new MusicalInstrumentCounter(new WaterGlassInstrument());

    for (int i = 0; i < 3; i++) {
      instrument.play();
      instrumentAdapted.play();
    }
    assertEquals(6, MusicalInstrumentCounter.getCount());

  }


  @Test
  public void testOrchestra() {
    Orchestra orchestra = new Orchestra();

    orchestra.add(new Trumpet());
    for (int i = 0; i < 4; i++) {
      orchestra.add(new Horn());
    }
    orchestra.add(new WaterGlassInstrument());
    orchestra.add(new GermanMusicInstrument(new IronRod()));

    String suonoTotale = orchestra.play();

    assertEquals("pepepe\npapapa\npapapa\npapapa\npapapa\ndiding\ntatang",
        suonoTotale);
  }

  @Test
  public void testSezioni() {
    Orchestra fiati = new Orchestra();
    Orchestra percussioni = new Orchestra();

    Orchestra totale = new Orchestra();

    fiati.add(new Trumpet());
    fiati.add(new Horn());

    percussioni.add(new WaterGlassInstrument());
    percussioni.add(new GermanMusicInstrument(new IronRod()));

    totale.add(fiati);
    totale.add(percussioni);

    String suonoTotale = totale.play();
    String suonoFiati = fiati.play();

    assertEquals("pepepe\n" +
        "papapa\n" +
        "diding\n" +
        "tatang", suonoTotale);
    assertEquals("pepepe\n" +
        "papapa", suonoFiati);
  }

  @Test
  public void testOrchestraContata() {
    Orchestra orchestra = new Orchestra();

    orchestra.add(new Trumpet());
    for (int i = 0; i < 4; i++) {
      orchestra.add(new Horn());
    }
    orchestra.add(new WaterGlassInstrument());
    orchestra.add(new GermanMusicInstrument(new IronRod()));

    MusicalInstrument orchestraSorvegliata = new MusicalInstrumentCounter(orchestra);
    String suonoTotale = orchestraSorvegliata.play();

    assertEquals("pepepe\npapapa\npapapa\npapapa\npapapa\ndiding\ntatang",
        suonoTotale);
    assertEquals(1, MusicalInstrumentCounter.getCount());

  }

  @Test
  public void testOrchestraConSingoliStrumentiContati() {
    Orchestra orchestra = new Orchestra();

    orchestra.add(new MusicalInstrumentCounter(new Trumpet()));
    for (int i = 0; i < 4; i++) {
      orchestra.add(new MusicalInstrumentCounter(new Horn()));
    }
    orchestra.add(new MusicalInstrumentCounter(new WaterGlassInstrument()));
    orchestra.add(new MusicalInstrumentCounter(new GermanMusicInstrument(new IronRod())));


    String suonoTotale = orchestra.play();

    assertEquals("pepepe\npapapa\npapapa\npapapa\npapapa\ndiding\ntatang",
        suonoTotale);
    assertEquals(7, MusicalInstrumentCounter.getCount());
  }

  @Test
  public void testOrchestraMixedCounting() {
    Orchestra orchestra = new Orchestra();

    orchestra.add(new Trumpet());
    for (int i = 0; i < 4; i++) {
      orchestra.add(new Horn());
    }
    orchestra.add(new MusicalInstrumentCounter(new WaterGlassInstrument()));
    orchestra.add(new MusicalInstrumentCounter(new GermanMusicInstrument(new IronRod())));


    MusicalInstrument orchestraContata = new MusicalInstrumentCounter(orchestra);
    String suonoTotale = orchestraContata.play();

    assertEquals("pepepe\npapapa\npapapa\npapapa\npapapa\ndiding\ntatang",
        suonoTotale);
    assertEquals(7, MusicalInstrumentCounter.getCount());
  }

  @Test
  public void testSezioniContate() {
    Orchestra fiati = new Orchestra();
    Orchestra percussioni = new Orchestra();

    Orchestra totale = new Orchestra();

    fiati.add(new Trumpet());
    fiati.add(new Horn());

    percussioni.add(new WaterGlassInstrument());
    percussioni.add(new GermanMusicInstrument(new IronRod()));

    totale.add(fiati);
    totale.add(percussioni);

    MusicalInstrument totaleContato = new MusicalInstrumentCounter(totale);
    String suonoTotale = totaleContato.play();
    String suonoFiati = fiati.play();

    assertEquals("pepepe\n" +
        "papapa\n" +
        "diding\n" +
        "tatang", suonoTotale);
    assertEquals("pepepe\n" +
        "papapa", suonoFiati);

    assertEquals(4, MusicalInstrumentCounter.getCount());
  }

}